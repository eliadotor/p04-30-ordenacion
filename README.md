**Elia Dotor Puente P04-30**

#Clase Ordenacion

###Primero creamos el proyecto *P04-30-Ordenacion*

Una vez creado, procedemos a la creación de la clase **Ordenacion** y escribiremos nuestro programa que contendrá un solo método que reciba tres números por pantalla y los muestre en orden de mayor a menor.

**Clase Ordenacion:**
![Clase_Ordenacion](img/ordenacion.png)

A continuación, pinchando sobre la clase, creamos una clase de prueba *New>>JUnit Test Case*, la herramienta nos crea la clase de prueba añadiendo las librerías de JUnit necesarias para su ejecución y un métedo de prueba vacío con la etiqueta *@Test*.

Necesitaremos hacer varios test para comprobar todos los casos de prueba.

Nuestro primer test se llama `testOrdenar3123()`, recibe tres numeros (num1, num2, num3), en este caso num1 es mayor que num2 y a su vez num2 es mayor que num3.

En el test `testOrdenar3132()`, recibe tres numeros (num1, num2, num3) y comprueba que  num1 es mayor que num2 y num3 es mayor que num2 y posteriormente comprueba que num1 es mayor que num3. 

En el test `testOrdenar3312()`, recibe tres numeros (num1, num2, num3) y comprueba que  num1 es mayor que num2 y num3 es mayor que num2 y posteriormente comprueba que num1 no es mayor que num3.


En el test `testOrdenar3231()`, recibe tres numeros (num1, num2, num3) y comprueba que  num2 es mayor que num1 y num3 es mayor que num1 y posteriormente comprueba que num2 es mayor que num3.

En el test `testOrdenar3321()`, recibe tres numeros (num1, num2, num3) y comprueba que  num2 es mayor que num1 y num3 es mayor que num1 y posteriormente comprueba que num2 no es mayor que num3.

En el test `testOrdenar3213()`, recibe tres numeros (num1, num2, num3) y comprueba que  num2 es mayor que num1 y num1 es mayor que num3.

**Clase OrdenacionTest:**
![Clase_TestOrdenacion](img/ordenacionTest1.png)
![Clase_TestOrdenacion](img/ordenacionTest2.png)


De esta forma, despues de implementar los casos de prueba con JUnit, podemos observar *(botón derecho sobre la clase --> coverage as)* que obtenemos una cobertura del 100%:

![Coverage](img/coverage100.png)

**En la carpeta doc incluimos el diagrama de flujo del código y la tabla de los casos de prueba.**

[Enlace al repositorio](https://bitbucket.org/eliadotor/p04-30-ordenacion)