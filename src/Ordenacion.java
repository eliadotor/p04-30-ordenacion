
public class Ordenacion {

	/**
	 * Método que recibe tres números y los ordena de mayor a menor
	 * @param num1
	 * @param num2
	 * @param num3
	 * @return una cadena que muestra los tres números ordenados
	 */
	public String ordenar3(int num1, int num2, int num3) {
		if(num1>=num2 && num2>=num3) {
		    return num1 + ", " + num2 + ", " + num3;
		    
		}else if(num1>=num2 && num3>=num2){
			
			if(num1>=num3) {
				return num1 + ", " + num3 + ", " + num2;

			}else {
				return num3 + ", " + num1 + ", " + num2;
			}
		}else if(num2>=num1 && num3>=num1) {
			if(num2>=num3) {
				return num2 + ", " + num3 + ", " + num1;

			}else {
				return num3 + ", " + num2 + ", " + num1;
			}
		}else {
			return num2 + ", " + num1 + ", " + num3;
		}
	}
}  