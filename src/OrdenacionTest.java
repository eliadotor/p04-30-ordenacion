import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class OrdenacionTest {
	
	@Test
	void testOrdenar3123() {
		String esperado = "18, 10, 4";
		Ordenacion ordenacion1 = new Ordenacion();
		int num1 = 18;
		int num2 = 10;
		int num3 = 4;
		String resultado = ordenacion1.ordenar3(num1, num2, num3);
		assertEquals(esperado, resultado);
	}
	
	@Test
	void testOrdenar3132() {
		String esperado = "8, 7, 2";
		Ordenacion ordenacion2 = new Ordenacion();
		int num1 = 8;
		int num2 = 2;
		int num3 = 7;
		String resultado = ordenacion2.ordenar3(num1, num2, num3);
		assertEquals(esperado, resultado);
	}

	@Test
	void testOrdenar3312() {
		String esperado = "5, 2, 1";
		Ordenacion ordenacion3 = new Ordenacion();
		int num1 = 2;
		int num2 = 1;
		int num3 = 5;
		String resultado = ordenacion3.ordenar3(num1, num2, num3);
		assertEquals(esperado, resultado);	
	}
	
	@Test
	void testOrdenar3231() {
		String esperado = "6, 5, 3";
		Ordenacion ordenacion4 = new Ordenacion();
		int num1 = 3;
		int num2 = 6;
		int num3 = 5;
		String resultado = ordenacion4.ordenar3(num1, num2, num3);
		assertEquals(esperado, resultado);	
	}
	
	@Test
	void testOrdenar3321() {
		String esperado = "8, 6, 4";
		Ordenacion ordenacion5 = new Ordenacion();
		int num1 = 4;
		int num2 = 6;
		int num3 = 8;
		String resultado = ordenacion5.ordenar3(num1, num2, num3);
		assertEquals(esperado, resultado);
	}

	@Test
	void testOrdenar3213() {
		String esperado = "9, 7, 3";
		Ordenacion ordenacion6 = new Ordenacion();
		int num1 = 7;
		int num2 = 9;
		int num3 = 3;
		String resultado = ordenacion6.ordenar3(num1, num2, num3);
		assertEquals(esperado, resultado);	
	}
	
}
